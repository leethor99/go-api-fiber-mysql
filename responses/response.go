package responses

import "github.com/gofiber/fiber/v2"

type MultiData struct {
	Status     string     `json:"status"`
	TotalData  int        `json:"total_data"`
	TotalPage  int        `json:"total_page"`
	CurentPage int        `json:"curent_page"`
	Data       *fiber.Map `json:"data"`
}

type SingleData struct {
	Status string     `json:"status"`
	Data   *fiber.Map `json:"data"`
}

type ExcelData struct {
	Status string     `json:"status"`
	Total  int        `json:"total"`
	Data   *fiber.Map `json:"data"`
}
