package controllers

import (
	"database/sql"
	"net/http"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/lithor99/go-api-fiber-mysql/configs"
	"github.com/lithor99/go-api-fiber-mysql/middlewares"
	"github.com/lithor99/go-api-fiber-mysql/models"
	"github.com/lithor99/go-api-fiber-mysql/responses"
)

type ProductData struct {
	Id        uint         `json:"id"`
	Name      string       `json:"name"`
	Price     float64      `json:"price"`
	Quantity  int          `json:"quantity"`
	CreatedAt time.Time    `json:"created_at"`
	UpdatedAt time.Time    `json:"updated_at"`
	DeletedAt sql.NullTime `json:"deleted_at"`
	Username  string       `json:"created_by"`
}

func CreateProduct(c *fiber.Ctx) error {
	product := new(models.Products)
	user_id, _ := strconv.Atoi(middlewares.GetUserIdFromToken(c))
	if user_id != 0 {

		//validate the request body
		if err := c.BodyParser(product); err != nil {
			return c.Status(http.StatusBadRequest).JSON(responses.SingleData{Status: "error", Data: &fiber.Map{"data": err.Error()}})
		}

		//use the validator library to validate required fields
		product.CreatedBy = uint(user_id)
		if validationErr := validate.Struct(product); validationErr != nil {
			return c.Status(http.StatusBadRequest).JSON(responses.SingleData{Status: "error", Data: &fiber.Map{"data": validationErr.Error()}})
		}

		newProduct := models.Products{
			Name:      product.Name,
			Price:     product.Price,
			Quantity:  product.Quantity,
			CreatedBy: product.CreatedBy,
		}

		configs.Database.Create(&newProduct)
		return c.Status(http.StatusCreated).JSON(responses.SingleData{Status: "success", Data: &fiber.Map{"data": newProduct}})
	}
	return c.Status(http.StatusUnauthorized).JSON(responses.SingleData{Status: "error", Data: &fiber.Map{"data": "You are not logged in"}})
}

func GetProducts(c *fiber.Ctx) error {
	// var products []models.Products
	// var user models.Users
	var datas []ProductData
	var count int64
	var totalPage int64
	page, _ := strconv.Atoi(c.Query("page"))
	limit, _ := strconv.Atoi(c.Query("limit"))

	if page == 0 {
		page = 1
	}
	if limit == 0 {
		limit = 10
	}

	configs.Database.Model(models.Products{}).Count(&count).Where("deleted_at IS NULL")
	if count%int64(limit) == 0 {
		totalPage = count / int64(limit)
	} else {
		totalPage = count/int64(limit) + 1
	}

	fields := "products.id, products.name, products.price, products.quantity, products.created_at, products.updated_at, products.deleted_at, users.username"
	joins := "left join users on users.id = products.created_by"
	configs.Database.Table("products").Select(fields).Offset(limit*page - limit).Limit(limit).Order("products.created_at desc").Joins(joins).Where("products.deleted_at IS NULL").Scan(&datas)

	// configs.Database.Offset(limit*page-limit).Limit(limit).Order("created_at desc").Find(&products).Where("deleted_at =?", nil)
	// for i := 0; i < len(products); i++ {
	// 	res := configs.Database.Find(&user, products[i].CreatedBy)
	// 	if res.RowsAffected == 0 {
	// 		return c.Status(http.StatusBadRequest).JSON(responses.SingleData{Status: "error", Data: &fiber.Map{"data": "no data available"}})
	// 	}
	// 	datas = append(datas, ProductData{products[i].ID, products[i].Name, products[i].Price, products[i].Quantity, products[i].CreatedAt, products[i].UpdatedAt, sql.NullTime(products[i].DeletedAt), user.Username})
	// }
	return c.Status(http.StatusOK).JSON(responses.MultiData{Status: "success", TotalData: int(count), TotalPage: int(totalPage), CurentPage: page, Data: &fiber.Map{"data": datas}})
}

func GetProduct(c *fiber.Ctx) error {
	id := c.Params("id")
	var data ProductData

	fields := "products.id, products.name, products.price, products.quantity, products.created_at, products.updated_at, products.deleted_at, users.username"
	joins := "left join users on users.id = products.created_by"
	configs.Database.Table("products").Select(fields).Joins(joins).Where("products.id =?", id).Scan(&data)

	// result := configs.Database.Find(&product, id)

	// if result.RowsAffected == 0 {
	// 	return c.Status(http.StatusBadRequest).JSON(responses.SingleData{Status: "error", Data: &fiber.Map{"data": "no data available"}})
	// }
	// res := configs.Database.Find(&user, product.CreatedBy)
	// if res.RowsAffected == 0 {
	// 	return c.Status(http.StatusBadRequest).JSON(responses.SingleData{Status: "error", Data: &fiber.Map{"data": "no data available"}})
	// }
	// data = productData{product.ID, product.Name, product.Price, product.Quantity, product.CreatedAt, product.UpdatedAt, sql.NullTime(product.DeletedAt), user}
	return c.Status(http.StatusOK).JSON(responses.SingleData{Status: "success", Data: &fiber.Map{"data": data}})
}

func UpdateProduct(c *fiber.Ctx) error {
	id := c.Params("id")
	product := new(models.Products)

	if err := c.BodyParser(product); err != nil {
		return c.Status(http.StatusBadRequest).JSON(responses.SingleData{Status: "error", Data: &fiber.Map{"data": err.Error()}})
	}
	configs.Database.Where("id = ?", id).Updates(&product)
	return c.Status(http.StatusOK).JSON(responses.SingleData{Status: "success", Data: &fiber.Map{"data": product}})
}

func DeleteProduct(c *fiber.Ctx) error {
	id := c.Params("id")
	var user models.Products

	configs.Database.Delete(&user, id)
	return c.Status(http.StatusOK).JSON(responses.SingleData{Status: "success", Data: &fiber.Map{"data": user}})
}
